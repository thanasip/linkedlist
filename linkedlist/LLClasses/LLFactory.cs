﻿using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;

namespace LinkedList.LLClasses {
    public static class LLFactory {
        public static LL<T> CreateInstanceFromRange<T>(T start, T end, bool reverse = false)
        where T : struct, IComparable, IComparable<T>, IEquatable<T> {
            LL<T> list = LL<T>.CreateEmptyInstance();
            for (dynamic i = start; i <= end; i++) {
                if (!reverse)
                    list.AppendElement(i);
                else {
                    list.PrependElement(i);
                }
            }
            return list;
        }

        public static LL<T> CreateInitialisedInstance<T>(T val, T len)
        where T : struct, IComparable, IComparable<T>, IEquatable<T> {
            LL<T> list = LL<T>.CreateEmptyInstance();
            for (dynamic i = 0; i < len; i++) {
                list.AppendElement(val);
            }
            return list;
        }

        public static LL<T> CreateRandomNumericInstance<T>(int minVal, int maxVal, long len)
        where T : struct, IComparable, IComparable<T>, IEquatable<T>, IFormattable {
            LL<T> list = LL<T>.CreateEmptyInstance();
            ConcurrentStack<T> stack = new ConcurrentStack<T>();
            Random r = new Random();

            Parallel.For(0, len, i => {
                stack.Push((dynamic)r.Next(minVal, maxVal + 1));
            });

            foreach(T t in stack) {
                list.AppendElement(t);
            }

            return list;
        }
    }
}
