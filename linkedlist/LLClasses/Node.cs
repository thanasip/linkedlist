﻿using System;

namespace LinkedList.LLClasses {
    public class Node<T> : IDisposable {
        public Node<T> Prev { get; set; }
        public Node<T> Next { get; set; }
        public T Value { get; set; }

        public static Node<T> CreateInstance (T val) {
            return new Node<T> {
                Prev = null,
                Next = null,
                Value = val
            };
        }

        public override string ToString() {
            return Value.ToString();
        }

        public void Dispose() {
            Prev = null;
            Next = null;
            Value = default(T);
        }

        private Node() { }
    }
}
