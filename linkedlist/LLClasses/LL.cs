﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LinkedList.LLClasses {
    public class LL<T> : IDisposable {
        public Node<T> Head { get; set; }
        public Node<T> Tail { get; set; }
        public uint Length { get; set; }

        public static LL<T> CreateInstance (T val) {
            Node<T> newNode = Node<T>.CreateInstance(val);
            return new LL<T> {
                Head = newNode,
                Tail = newNode,
                Length = 1
            };
        }

        public static LL<T> CreateEmptyInstance() {
            return new LL<T> {
                Head = null,
                Tail = null,
                Length = 0
            };
        }

        public override string ToString() {
            if (Length == 0) {
                return "[]";
            } else {
                StringBuilder sb = new StringBuilder();
                sb.Append("[");
                Node<T> currNode = Head;
                uint i = 0;
                do {
                    if (i < Length - 1)
                        sb.Append($"{currNode.Value.ToString()}, ");
                    else
                        sb.Append($"{currNode.Value.ToString()}]");
                    i++;
                } while ((currNode = currNode.Next) != null);
                return sb.ToString();
            }
        }

        public static T ConvertValue<T,U>(U value) where U : IConvertible {
            return (T) Convert.ChangeType(value, typeof(T));
        }

        public dynamic this[int index] {
            get {
                return GetNodeAt(index);
            }
            set {
                ReplaceElementAt(value, (uint) index);
            }
        }

        public void PrettyPrintList() {
            if (Length == 0) {
                Console.WriteLine("Empty List!");
            } else {
                Node<T> currNode = Head;
                uint i = 0;
                do {
                    Console.WriteLine($"({i++}): {currNode.Value.ToString()}");
                } while ((currNode = currNode.Next) != null);
            }
        }

        public void InsertElement(T val, uint pos) {
            if (Length == 0 && pos == 0) {
                Node<T> newNode = Node<T>.CreateInstance(val);
                Head = newNode;
                Tail = newNode;
                Length = 1;
            } else {
                if (pos < 0) throw new ArgumentException("Cannot insert at negative index");
                else if (pos == 0) PrependElement(val);
                else if (pos > Length) throw new ArgumentException("Position index exceeds list length.");
                else if (pos == Length) AppendElement(val);
                else { //Find it
                    Node<T> newNode = Node<T>.CreateInstance(val);
                    if (pos >= Length / 2) { //If we're closer to the tail, start from there and go backwards
                        uint posFromEnd = (Length - pos) - 1;
                        Node<T> currNode = Tail;
                        for (uint i = 0; i < posFromEnd; i++)
                            currNode = currNode.Prev;
                        newNode.Next = currNode;
                        newNode.Prev = currNode.Prev;
                        currNode.Prev.Next = newNode;
                        currNode.Prev = newNode;
                        Length += 1;
                    } else { //Else go from the head
                        Node<T> currNode = Head;
                        for (uint i = 0; i < pos; i++)
                            currNode = currNode.Next;
                        newNode.Next = currNode;
                        newNode.Prev = currNode.Prev;
                        currNode.Prev.Next = newNode;
                        currNode.Prev = newNode;
                        Length += 1;
                    }
                }
            }
        }

        public void ReplaceElementAt(T val, uint pos) {
            if (pos < 0) throw new ArgumentException("Cannot insert at negative index");
            else if (pos == 0) Head.Value = val;
            else if (pos > Length) throw new ArgumentException("Position index exceeds list length.");
            else if (pos == Length) Tail.Value = val;
            else { //Find it
                Node<T> newNode = Node<T>.CreateInstance(val);
                if (pos >= Length / 2) { //If we're closer to the tail, start from there and go backwards
                    uint posFromEnd = (Length - pos) - 1;
                    Node<T> currNode = Tail;
                    for (uint i = 0; i < posFromEnd; i++)
                        currNode = currNode.Prev;
                    currNode.Value = val;
                } else { //Else go from the head
                    Node<T> currNode = Head;
                    for (uint i = 0; i < pos; i++)
                        currNode = currNode.Next;
                    currNode.Value = val;
                }
            }
        }

        public void RemoveElementAt(long pos) {
            if (pos == 0) {
                Head.Next.Prev = null;
                Head = Head.Next;
                Length -= 1;
            } else if (pos == Length - 1) {
                Tail.Prev.Next = null;
                Tail = Tail.Prev;
                Length -= 1;
            } else {
                Node<T> currNode = Head;
                for (long i = 0; i < pos; i++)
                    currNode = currNode.Next;
                currNode.Prev.Next = currNode.Next;
                currNode.Next.Prev = currNode.Prev;
                Length -= 1;
            }
        }

        public void RemoveFirstOfElement(T val) {
            RemoveElementAt(FindFirstOfElement(val));
        }

        public void RemoveLastOfElement(T val) {
            RemoveElementAt(FindLastOfElement(val));
        }

        public void RemoveAllOfElement(T val) {
            long count = GetCountOfElement(val);
            for (int i = 0; i < count; i++)
                RemoveFirstOfElement(val);
        }

        public long[] FindAllOfElement(T val) {
            List<long> positions = new List<long>();
            long i = 0;
            Node<T> currNode = Head;
            do {
                if (currNode.Value.Equals(val))
                    positions.Add(i);
                i++;
            } while ((currNode = currNode.Next) != null);
            return (positions.Count > 0) ? positions.ToArray() : new long[] { -1 };
        }

        public long GetCountOfElement(T val) {
            long i = 0;
            Node<T> currNode = Head;
            do {
                if (currNode.Value.Equals(val))
                    i++;
            } while ((currNode = currNode.Next) != null);
            return i;
        }

        public long FindFirstOfElement(T val) {
            long i = 0;
            bool found = false;
            Node<T> currNode = Head;
            do {
                if (currNode.Value.Equals(val)) {
                    found = true;
                    break;
                } i++;
            } while ((currNode = currNode.Next) != null);
            return (found) ? i : -1;
        }

        public long FindLastOfElement(T val) {
            long i = Length - 1;
            bool found = false;
            Node<T> currNode = Tail;
            do {
                if (currNode.Value.Equals(val)) {
                    found = true;
                    break;
                } i--;
            } while ((currNode = currNode.Prev) != null);
            return (found) ? i : -1;
        }

        public T Max() {
            Node<T> currNode = Head.Next;
            dynamic max = Head.Value;
            do {
                if ((dynamic) currNode.Value > max)
                    max = (dynamic) currNode.Value;
            } while ((currNode = currNode.Next) != null);
            return max;
        }

        public T Min() {
            Node<T> currNode = Head.Next;
            dynamic max = Head.Value;
            do {
                if ((dynamic) currNode.Value < max)
                    max = (dynamic) currNode.Value;
            } while ((currNode = currNode.Next) != null);
            return max;
        }

        public Node<T> GetNodeAt(long pos) {
            if (pos < 0) throw new ArgumentException("Position can't be less than 0");
            if (pos == 0) return Head;
            if (pos == Length - 1) return Tail;
            if (pos >= Length) throw new ArgumentException("Index greater than end of list.");

            if (pos >= Length / 2) {
                Node<T> currNode = Tail;
                long posFromEnd = (Length - pos) - 1;
                for (long i = 0; i < posFromEnd; i++, currNode = currNode.Prev) ;
                return currNode;
            } else {
                Node<T> currNode = Head;
                for (long i = 0; i < pos; i++, currNode = currNode.Next) ;
                return currNode;
            }
        }

        public Node<T> GetFirstNodeWithValue(T val) {
            return GetNodeAt(FindFirstOfElement(val));
        }

        public Node<T> GetLastNodeWithValue(T val) {
            return GetNodeAt(FindLastOfElement(val));
        }

        public Node<T>[] GetAllNodesWithValue(T val) {
            List<Node<T>> list = new List<Node<T>>();
            long[] positions = FindAllOfElement(val);
            foreach (long pos in positions)
                list.Add(GetNodeAt(pos));
            return list.ToArray();
        }

        public void PrependElement(T val) {
            if (Length == 0) {
                InsertElement(val, 0);
            } else {
                Node<T> newNode = Node<T>.CreateInstance(val);
                newNode.Next = Head;
                Head.Prev = newNode;
                Head = newNode;
                Length += 1;
            }
        }

        public void AppendElement(T val) {
            if (Length == 0) {
                InsertElement(val, 0);
            } else {
                Node<T> newNode = Node<T>.CreateInstance(val);
                newNode.Prev = Tail;
                Tail.Next = newNode;
                Tail = newNode;
                Length += 1;
            }
        }

        public void AppendLinkedList(LL<T> list) {
            if (list.Head == Head) throw new ArgumentException("You can't append a Linked List to itself.");
            Tail.Next = list.Head;
            list.Head.Prev = Tail;
            Tail = list.Tail;
            Length += list.Length;
        }

        public void Dispose() {
            while(Head != null) {
                Node<T> c = Head;
                Head = Head.Next;
                c.Dispose();
            }
            GC.Collect();
        }

        private LL() { }
    }
}
