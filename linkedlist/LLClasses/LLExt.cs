﻿using System;

namespace LinkedList.LLClasses {
    public static class LLExt {
        public static T Fold<T>(this LL<T> ll, Func<T, T, T> func)
        where T : struct, IComparable, IComparable<T>, IEquatable<T>, IFormattable {
            if (ll.Length <= 0) return new T();
            if (ll.Length == 1) return ll.Head.Value;
            T i = (dynamic) ll.Head.Value;
            Node<T> currNode = ll.Head.Next;
            do {
                i = func(i, currNode.Value);
            } while ((currNode = currNode.Next) != null);
            return i;
        }
    }
}
