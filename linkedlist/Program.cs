﻿using System;
using System.Numerics;
using LinkedList.LLClasses;

namespace LinkedList {
    class Program {
        static void Main(string[] args) {
            LL<BigInteger> list = LLFactory.CreateInstanceFromRange<BigInteger>(0,99);
            Console.WriteLine($"{list.Max()} Sum: Fold (+) over {list}");
            Console.WriteLine($"list.Fold((x, y) => x + y): {list.Fold((x, y) => x + y)}");
            Console.WriteLine($"Get Node at 7: {list[7]}");
            list[7] = 777;
            Console.WriteLine($"Get Node at 7: {list[7]}");
            Console.ReadKey(true);
        }
    }
}
